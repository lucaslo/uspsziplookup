import openpyxl
from pathlib import Path
from lookUp import searchZip

inputFolder=Path("./inputExcel")

workBooksPath = [excelFile for excelFile in inputFolder.iterdir() if excelFile.suffix==".xlsx"]

def findZipCell(iterRows):
    for row in iterRows():
        for cell in row:
            if cell.value=="ZIP":
                return cell

def processWorkbook(workbookPath):
    workBook = openpyxl.load_workbook(workBookPath)
    currentSpreadsheet = workBook.active
    zipCell = findZipCell(currentSpreadsheet.iter_rows)
    cityCol = zipCell.column+1
    stateCol =zipCell.column+2
    for row in range(zipCell.row+1,currentSpreadsheet.max_row):
            zip= currentSpreadsheet.cell(row,zipCell.column).value
            try:
                zipInfo=searchZip(zip)
                currentSpreadsheet.cell(row,cityCol).value=zipInfo["city"]
                currentSpreadsheet.cell(row,stateCol).value=zipInfo["state"]
            except Exception as err:
                print(err)
    workBook.save(workBookPath)

for workBookPath in workBooksPath:
    processWorkbook(workBookPath)