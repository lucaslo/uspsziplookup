from selenium import webdriver
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

browser = webdriver.Firefox('/usr/local/bin')
outputCss = "#cityByZipDiv > div.row.col-md-5.col-sm-12.col-xs-12.recommended-cities > p.row-detail-wrapper"

def parseCityAndState(cityAndState):
    cityAndState = cityAndState[0].text
    stringSize=len(cityAndState)
    state=cityAndState[stringSize-2:]
    city=cityAndState[:stringSize-3]
    return{"state":state, "city":city}

def typeWithDelay(text,element):
    for character in text: 
        element.send_keys(character)

def waitInfoPageToLoad():
    try:
        wait = WebDriverWait(browser, 5)
        wait.until(ec.visibility_of_element_located((By.CSS_SELECTOR, outputCss)))
    except TimeoutException :
        print("Could not find element in alocated time")
        raise Exception("Could not find information for this zip -- err1")

def getCityAndState():
    cityAndState = browser.find_elements_by_css_selector(outputCss)
    if(len(cityAndState)>0):
        return parseCityAndState(cityAndState)
    else :
        raise Exception("Could not find information for this zip-- err2")
    
def searchZip(zip):
    browser.get('https://tools.usps.com/zip-code-lookup.htm?citybyzipcode')
    zipInputBox = browser.find_element_by_id("tZip")
    typeWithDelay(str(zip), zipInputBox)
    findButton = browser.find_element_by_id("cities-by-zip-code")
    findButton.click()
    waitInfoPageToLoad()    
    return getCityAndState()